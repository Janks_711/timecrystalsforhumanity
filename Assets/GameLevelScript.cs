﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLevelScript : LevelScript {

    public AudioClip levelMusic;

    private void Awake()
    {
        base.AwakeScript();
    }

    // Use this for initialization
    void Start () {

        OnLevelLoad();

        AudioSource music = AudioManager.audioManager.PushSource("level-music", levelMusic, true);
        music.Play();
    }
	


	// Update is called once per frame
	void Update () {
		
	}
}