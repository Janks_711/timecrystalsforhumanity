﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystal : MonoBehaviour {

	// Use this for initialization
	void Start () {
		


	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //Pickup the crystal.
            GameManager.gameManager.crystalCount.crystalCount++;
            Destroy(gameObject);


        }
    }

    // Update is called once per frame
    void Update () {

        transform.localScale = new Vector3(Mathf.PingPong(Time.time, 0.5f), Mathf.PingPong(Time.time, 0.5f));

    }
}

