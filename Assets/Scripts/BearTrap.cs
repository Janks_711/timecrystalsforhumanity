﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearTrap : TrapBase {

    public float ouchieTime = 2.0f;
    
    public Sprite openSprite;
    public Sprite closeSprite;

    public List<AudioClip> hurtSounds = new List<AudioClip>();

    public bool activated = false;

    private SpriteRenderer spriteRenderer;

    public override void Start() 
    {
        base.Start();

        spriteRenderer = GetComponent<SpriteRenderer>();
        //add this to the list of things to do when the player enters the trap.
        onTrapped += PausePlayerMove;
    }

    public IEnumerator FreezePlayer(float freezeSeconds, Brother b)
    {
        Debug.Log(b.name);
        b.characterMove.frozen = true;
        spriteRenderer.sprite = closeSprite;

        string hashedName = "hurt-" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff").GetHashCode();

        int randomSound = UnityEngine.Random.Range(0, hurtSounds.Count - 1);

        AudioSource hurt = AudioManager.audioManager.PushSource(hashedName, hurtSounds[randomSound]);
        hurt.Play();

        yield return new WaitForSeconds(freezeSeconds);

        AudioManager.audioManager.RemoveSource(hashedName);

        activated = true;
        b.characterMove.frozen = false;
    }

    public void PausePlayerMove(Brother b)
    {
        if (activated == false)
        {
            StartCoroutine(FreezePlayer(ouchieTime, b));
        }
    }


}
