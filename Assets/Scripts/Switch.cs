﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SpriteRenderer))]
public class Switch : MonoBehaviour {

    public UnityEvent onSwitchStateChange;

    public AudioClip switchSound;

    bool playerCanInteract = false;

    public bool isActive = false;

    public Sprite onSprite;
    public Sprite offSprite;

    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerCanInteract = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerCanInteract = false;
        }

    }

    private void SwitchSprite()
    {
        if (isActive)
        {
            //switch to close sprite.
            spriteRenderer.sprite = offSprite;
            isActive = false;
        }
        else
        {
            //switch to open sprite.
            spriteRenderer.sprite = onSprite;
            isActive = true;
        }

        string hashedTimeName = "lever-sound-" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff").GetHashCode();

        Debug.Log(hashedTimeName);

        AudioSource source = AudioManager.audioManager.PushSource(hashedTimeName, switchSound);
        source.Play();

        StartCoroutine(RemoveAfterPlay(hashedTimeName, source));
    }

    private IEnumerator RemoveAfterPlay(string name, AudioSource source)
    {
        Debug.Log(source.clip.length);
        yield return new WaitForSeconds(source.clip.length);

        Debug.Log("Removing now");
        AudioManager.audioManager.RemoveSource(name);
    }

    // Update is called once per frame
    void Update () {
		
        if(playerCanInteract)
        {
            if(Input.GetKeyDown(KeyCode.E))
            {
                //The player interacted.

                SwitchSprite();

                onSwitchStateChange.Invoke();

            }
        }

	}
}
