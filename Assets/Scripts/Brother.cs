﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMovement))]
[System.Serializable]
public class Brother : MonoBehaviour {

    public CharacterMovement characterMove;

	// Use this for initialization
	void Start () {
        characterMove = GetComponent<CharacterMovement>();
	}
}
