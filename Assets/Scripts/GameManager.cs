﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager gameManager;

    public List<Brother> brothers = new List<Brother>();
    public CrystalCounter crystalCount;
    public Timer timer;
    
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        gameManager = this;

    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
