﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuLevelScript : LevelScript {

    public AudioClip musicClip; 

	// Use this for initialization
	void Start () {

        OnLevelLoad();
        AudioSource music = AudioManager.audioManager.PushSource("main-menu-music", musicClip, true);
        music.Play();
    }

    private void Awake()
    {
        base.AwakeScript();
    }

    // Update is called once per frame
    void Update () {
		
        

	}
}
